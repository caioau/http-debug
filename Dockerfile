FROM golang:1.23-bookworm AS builder

RUN groupadd --gid 1000 scratchuser \
    && useradd -u 1000 -g 1000 scratchuser

WORKDIR /usr/local/go/src/app

COPY app.go .

RUN CGO_ENABLED=0 go build -ldflags="-s -w" app

# ---------------------------------------------------------

FROM scratch

COPY --from=builder /etc/passwd /etc/passwd 
# trick to run rootless from scratch
COPY --from=builder /usr/local/go/src/app/app /usr/local/bin/

USER scratchuser 
# the user was created from builder stage and copied the /etc/passwd

#ENV port=80

ENTRYPOINT [ "app" ]

EXPOSE 5000
