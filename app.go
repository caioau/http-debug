package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/user"
	"strings"
)

type r_info struct {
	Method  string            `json:"method"`
	Proto   string            `json:"proto"`
	Url     string            `json:"Url"`
	Host    string            `json:"Host"`
	User    string            `json:"User"`
	Addr    string            `json:"addr"`
	Body    string            `json:"Body"`
	Headers map[string]string `json:"headers"`
	EnvVars map[string]string `json:"EnvVars"`
}

func ResponseHandler(w http.ResponseWriter, req *http.Request) {

	log.Printf("%v", req) // logs the requests to stdin

	type keyval map[string]string

	hs := keyval{}
	// go thro all the received headers appeding to our list
	if len(req.Header) > 0 {
		for name, headers := range req.Header {
			for _, h := range headers {
				h := h
				hs[name] = h
			}
		}
	} else {
		hs = nil
	}

	envVars := keyval{}
	// go thro all enviroment variables appeding to our list
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		envVars[pair[0]] = pair[1]
	}

	defer req.Body.Close()
	body, _ := io.ReadAll(req.Body)

	user, _ := user.Current() // current user, to make sure is (not) running as root
	userStr := "Username = " + user.Username + " uid = " + user.Uid + " gid = " + user.Gid

	finalHost := "nil"
	if len(req.Host) > 0 {
		finalHost = req.Host
	}

	r := &r_info{Method: req.Method, Proto: req.Proto, Addr: req.RemoteAddr,
		Headers: hs, EnvVars: envVars, Body: string(body), User: userStr,
		Host: finalHost, Url: req.RequestURI}

	b, _ := json.MarshalIndent(r, "", "   ")

	// set the content-type header so clients know to expect json
	w.Header().Set("Content-Type", "application/json")

	fmt.Fprintf(w, string(b))
}

func main() {

	port := "5000" // default listen port

	portEnv, portEnvPresent := os.LookupEnv("port")

	if portEnvPresent {
		log.Printf("debug: port variable present: %v\n", portEnv)
		port = portEnv
	}

	http.HandleFunc("/", ResponseHandler)

	log.Println("Listening on 0.0.0.0 port (override by passing a port env var)", port)

	log.Fatal(http.ListenAndServe(":"+port, nil))
}
