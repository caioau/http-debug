# http-debug

A tiny golang webserver to debug HTTP connections

When configuring a reverse proxy (like nginx or traefik) on docker-compose or a ingress on Kubernetes I like to use a small program to test the configuration, traefik has a program for that [traefik/whoami](https://github.com/traefik/whoami), but since I'm learning golang I wanted to create my own.

To deploy it, just use the docker-compose:

> docker-compose up --scale http-debug=5 -d 

This will create 5 instances of the http-debug, to test the load balancing (watch how the hostname change every request).

## Notes:

Here a some things I find interesting

* ✨ golang final binary can be compiled statically, so the binary contains everything (don't forget to pass the `CGO_ENABLED=0` environment variable during build), this way I used multi-stage build in the [Dockerfile](Dockerfile), so the final image just have the binary (~5MB)
* ✨ build with multi-arch support is so easy (but takes long time to build :/), the [buildx](https://github.com/docker/buildx/) makes everything so easy, see this in action in the [.gitlab-ci.yml](.gitlab-ci.yml).
